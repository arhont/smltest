﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace SoftMediaLab.Models;

public class Student
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    public string FullName { get; set; }
    public DateTime? BirthDate { get; set; }
    public Performance Performance { get; set; }
}

public enum Performance
{
    [Description("Неуд")] Неуд = 2,
    [Description("Удовлетворительно")] Удовлетворительно = 3,
    [Description("Хорошо")] Хорошо = 4,
    [Description("Отлично")] Отлично = 5
}
