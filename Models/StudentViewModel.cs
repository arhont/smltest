﻿using System;
using System.Collections.Generic;

namespace SoftMediaLab.Models;

public class StudentViewModel
{
    public int Id { get; init; }
    public string FullName { get; init; }
    public DateTime BirthDate { get; init; }
    public string Performance { get; init; }
}

public class StudentListViewModel
{
    public List<StudentViewModel> Items { get; set; }
}
