﻿using Newtonsoft.Json;
using RestSharp;
using SoftMediaLab.Models;

namespace ApiClient;

public class StudentApiClient : IStudentApiClient
{
    private readonly RestClient _client;

    public StudentApiClient(string baseUrl)
    {
        _client = new RestClient(baseUrl);
    }

    private static string Base() => "/Student";
    private static string ById(int id) => $"/Student/{id}";

    /// <inheritdoc />
    public async Task<List<Student>> GetAsync()
    {
        var request = new RestRequest(Base());
        var response = await _client.GetAsync(request);

        if (!response.IsSuccessStatusCode || response.Content is null)
        {
            return new List<Student>();
        }

        var result = JsonConvert.DeserializeObject<List<Student>>(response.Content);
        return result ?? new List<Student>();
    }

    /// <inheritdoc />
    public async Task<Student> GetAsync(int id)
    {
        var request = new RestRequest(ById(id));
        var response = await _client.GetAsync(request);

        response.ThrowIfError();

        if (response.Content is null)
        {
            return null;
        }

        var result = JsonConvert.DeserializeObject<Student>(response.Content);
        return result;
    }

    /// <inheritdoc />
    public async Task Update(StudentViewModel student)
    {
        var request = new RestRequest(ById(student.Id), Method.Put);
        request.AddJsonBody(student);

        var response = await _client.PutAsync(request);

        response.ThrowIfError();
    }

    /// <inheritdoc />
    public async Task Create(StudentViewModel student)
    {
        var request = new RestRequest(Base(), Method.Post);
        request.AddJsonBody(student);

        var response = await _client.PostAsync(request);

        response.ThrowIfError();
    }

    /// <inheritdoc />
    public async Task Delete(int id)
    {
        var request = new RestRequest(ById(id), Method.Delete);
        var response = await _client.DeleteAsync(request);

        response.ThrowIfError();
    }
}
