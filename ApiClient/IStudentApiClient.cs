﻿using SoftMediaLab.Models;

namespace ApiClient;
public interface IStudentApiClient
{
    /// <summary>
    /// Метод для получения всех студентов в системе
    /// </summary>
    /// <returns></returns>
    Task<List<Student>> GetAsync();
    /// <summary>
    /// Метод для получения конкретного студента
    /// </summary>
    /// <param name="id">Идентификатор студента в системе</param>
    /// <returns></returns>
    Task<Student> GetAsync(int id);
    /// <summary>
    /// Метод для изменения информации о студенте
    /// </summary>
    /// <param name="student">Модель студента</param>
    /// <returns></returns>
    Task Update(StudentViewModel student);
    /// <summary>
    /// Метод для заведения нового студента в системе
    /// </summary>
    /// <param name="student">Модель студента</param>
    /// <returns></returns>
    Task Create(StudentViewModel student);
    /// <summary>
    /// Метод для удаления студента из системы
    /// </summary>
    /// <param name="id">Идентификатор студента</param>
    /// <returns></returns>
    Task Delete(int id);
}
