﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftMediaLabView.Models
{
    public class StudentListViewModel
    {
        public List<StudentViewModel> Items { get; set; }
    }
}
