﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SoftMediaLabView.Models
{
    public class StudentViewModel
    {
        public StudentViewModel()
        {
            Performances = Enum.GetValues(typeof(Performance))
                .Cast<Performance>()
                .Select(r => new SelectListItem()
                {
                    Text = r.ToString(),
                    Value = r.ToString()
                })
                .ToList();
        }

        public Student Student { get; init; }

        public List<SelectListItem> Performances { get; init; }
    }

    public class Student
    {
        public int Id { get; init; }
        public string FullName { get; init; }
        public DateTime BirthDate { get; init; }
        public string Performance { get; init; }

        public Student()
        {

        }
        public Student(SoftMediaLab.Models.Student student)
        {
            Id = student.Id;
            FullName = student.FullName;
            BirthDate = student.BirthDate.GetValueOrDefault();
            Performance = student.Performance.ToString();
        }
    }

    public enum Performance
    {
        [Description("Неуд")]
        Неуд = 2,
        [Description("Удовлетворительно")]
        Удовлетворительно = 3,
        [Description("Хорошо")]
        Хорошо = 4,
        [Description("Отлично")]
        Отлично = 5
    }
}
