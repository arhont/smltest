﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SoftMediaLabView.Models;
using ApiClient;

namespace SoftMediaLabView.Controllers;

[Route("[controller]")]
public class StudentViewController : Controller
{
    private readonly IStudentApiClient _service;

    public StudentViewController(IStudentApiClient service)
    {
        _service = service;
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Index(int id)
    {
        var model = await _service.GetAsync(id);

        var vm = new StudentViewModel() { Student = new Student(model) };

        return View(vm);
    }

    [HttpGet("/")]
    public async Task<IActionResult> List()
    {
        var items = (await _service.GetAsync())
            .Select(r => new StudentViewModel()
            {
                Student = new Student(r)
            })
            .ToList();

        var model = new StudentListViewModel()
        {
            Items = items
        };

        return View(model);
    }

    [HttpGet("/Delete/{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        await _service.Delete(id);

        return RedirectToAction("List");
    }

    [HttpPost("/Update")]
    public async Task<ActionResult> Update(StudentViewModel model)
    {
        await _service.Update(new SoftMediaLab.Models.StudentViewModel()
        {
            BirthDate = model.Student.BirthDate,
            Id = model.Student.Id,
            FullName = model.Student.FullName,
            Performance = model.Student.Performance
        });

        return RedirectToAction("List");
    }

    [HttpGet("/Create")]
    public ActionResult Create()
    {
        var model = new StudentViewModel()
        {
            Student = new Student()
            {
                Performance = "0",
                BirthDate = DateTime.Now,
                FullName = ""
            }
        };

        return View(model);
    }

    [HttpPost("/Create")]
    public async Task<ActionResult> Create(StudentViewModel model)
    {
        await _service.Create(new SoftMediaLab.Models.StudentViewModel()
        {
            BirthDate = model.Student.BirthDate,
            FullName = model.Student.FullName,
            Performance = model.Student.Performance
        });

        return RedirectToAction("List");
    }
}
