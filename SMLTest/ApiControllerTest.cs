﻿using System;
using System.Net;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using SoftMediaLab.Controllers;
using SoftMediaLab.Infrastructure;
using SoftMediaLab.Models;

namespace SMLTest
{
    internal class ApiControllerTest
    {
        private Mock<IMediator> _mediator;
        private Mock<IStudentQuery> _query;

        [SetUp]
        public void Setup()
        {
            _mediator = new Mock<IMediator>();
            _query = new Mock<IStudentQuery>();
        }

        [Test]
        public void CreateStudentAsync_CorrectRequest_ReturnsCreated()
        {
            var student = new StudentViewModel()
            {
                Performance = Performance.Неуд.ToString(),
                FullName = "Test",
                BirthDate = new DateTime(2020, 12, 9)
            };

            _mediator.Setup(q => q.Send(It.IsAny<IRequest<bool>>(), default)).ReturnsAsync(true);

            var controller = new StudentController(_query.Object, _mediator.Object);

            var result = controller.Create(student).Result;

            Assert.IsInstanceOf<CreatedAtActionResult>(result);
        }

        [Test]
        public void CreateStudentAsync_IncorrectRequestData_ReturnsBadRequest()
        {
            var student = new StudentViewModel()
            {
                Performance = Performance.Неуд.ToString(),
                FullName = null,
                BirthDate = new DateTime(2020, 12, 9)
            };

            _mediator.Setup(q => q.Send(It.IsAny<IRequest<bool>>(), default)).ReturnsAsync(true);

            var controller = new StudentController(_query.Object, _mediator.Object);

            var result = controller.Create(student).Result;

            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void CreateStudentAsync_ServerFailure_ReturnsInternalServerError()
        {
            var student = new StudentViewModel()
            {
                Performance = Performance.Неуд.ToString(),
                FullName = "Test",
                BirthDate = new DateTime(2020, 12, 9)
            };

            var exceptionMessage = "Some internal server error";

            _mediator.Setup(q => q.Send(It.IsAny<IRequest<bool>>(), default)).ThrowsAsync(new Exception(exceptionMessage));
            var controller = new StudentController(_query.Object, _mediator.Object);

            var result = controller.Create(student).Result;

            Assert.IsInstanceOf<ObjectResult>(result);
            var value = ((ObjectResult)result).Value;
            if (value != null)
            {
                Assert.AreEqual(exceptionMessage, value.ToString());
            }

            Assert.AreEqual((int)HttpStatusCode.InternalServerError, ((ObjectResult)result).StatusCode);
        }

        [Test]
        public void UpdateStudentAsync_CorrectRequest_ReturnsNoContent()
        {
            var student = new StudentViewModel()
            {
                Performance = Performance.Неуд.ToString(),
                FullName = "Test",
                BirthDate = new DateTime(2020, 12, 9),
                Id = 1
            };

            _mediator.Setup(q => q.Send(It.IsAny<IRequest<bool>>(), default)).ReturnsAsync(true);
            _query.Setup(q => q.GetAsync(It.IsAny<int>())).ReturnsAsync(student);

            var controller = new StudentController(_query.Object, _mediator.Object);

            var result = controller.Update(student, student.Id).Result;

            Assert.IsInstanceOf<NoContentResult>(result);
        }

        [Test]
        public void UpdateStudentAsync_NonExistentEntity_ReturnsNotFound()
        {
            var student = new StudentViewModel()
            {
                Performance = Performance.Неуд.ToString(),
                FullName = "Test",
                BirthDate = new DateTime(2020, 12, 9),
                Id = 1
            };

            _query.Setup(q => q.GetAsync(1)).ReturnsAsync((StudentViewModel)null);

            var controller = new StudentController(_query.Object, _mediator.Object);

            var result = controller.Update(student, student.Id).Result;

            Assert.IsInstanceOf<NotFoundResult>(result);
        }
    }
}
