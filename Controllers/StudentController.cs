﻿using System;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SoftMediaLab.Commands;
using SoftMediaLab.Infrastructure;
using SoftMediaLab.Models;

namespace SoftMediaLab.Controllers;

[Route("api/[controller]")]
[ApiController]
public class StudentController : ControllerBase
{
    private readonly IStudentQuery _query;
    private readonly IMediator _mediator;

    public StudentController(IStudentQuery query, IMediator mediator)
    {
        _query = query;
        _mediator = mediator;
    }

    private static bool Check(StudentViewModel student)
    {
        return student != null && !string.IsNullOrEmpty(student.FullName) && !student.BirthDate.Equals(DateTime.MinValue);
    }

    /// <summary>
    /// Получение списка всех студентов
    /// </summary>
    /// <returns>Список студентов</returns>
    [HttpGet]
    public async Task<ActionResult> GetAll()
    {
        var items = await _query.GetAsync();
        return Ok(items);
    }

    /// <summary>
    /// Получение конкретного студента по идентификатор
    /// </summary>
    /// <param name="id">Идентификатор студента</param>
    /// <returns>Модель студента</returns>
    [HttpGet("{id}")]
    public async Task<ActionResult> GetById(int id)
    {
        var item = await _query.GetAsync(id);

        if (item == null)
        {
            return NotFound();
        }

        return Ok(item);
    }

    /// <summary>
    /// Мето для заведения нового студента в системе
    /// </summary>
    /// <param name="student">Модель нового студента</param>
    /// <returns>Ссылка на свежесозданную модель</returns>
    /// <response code="201">Студент создан</response>
    /// <response code="400">Запрос не прошёл проверку</response>
    /// <response code="500">Произошла внутренняя ошибка сервера</response>
    [HttpPost]
    public async Task<ActionResult> Create(StudentViewModel student)
    {
        if (!Check(student))
        {
            return BadRequest();
        }

        var createCommand = new CreateStudentCommand(student.FullName, student.Performance, student.BirthDate);

        try
        {
            var result = await _mediator.Send(createCommand);

            if (result)
            {
                return CreatedAtAction("GetById", "Student", new { @id = student.Id }, student);
            }

            return StatusCode((int)HttpStatusCode.InternalServerError);
        }
        catch (Exception ex)
        {
            return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
        }
    }

    /// <summary>
    /// Метод для обновления модели студента в системе
    /// </summary>
    /// <param name="student">Модель для обновления</param>
    /// <param name="id">Id студента</param>
    /// <returns>Обновление прошло успешно</returns>ъ
    /// <response code="204">Студент успешно обновлён</response>
    /// <response code="400">Запрос не прошёл проверку</response>
    /// <response code="404">Студент не найден</response>
    /// <response code="500">Внутренняя ошибка сервера</response>
    [HttpPut("{id}")]
    public async Task<ActionResult> Update([FromBody] StudentViewModel student, int id)
    {
        if (!Check(student))
        {
            return BadRequest();
        }

        var exist = await _query.GetAsync(student.Id);

        if (exist == null)
        {
            return NotFound();
        }

        var command = new UpdateStudentCommand(student.Id, student.FullName, student.Performance, student.BirthDate);

        try
        {
            var result = await _mediator.Send(command);

            return result ? NoContent() : StatusCode((int)HttpStatusCode.InternalServerError);
        }
        catch (Exception ex)
        {
            return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
        }
    }

    /// <summary>
    /// Метод для удаления студента из системы
    /// </summary>
    /// <param name="id">Идентификатор студента в системе</param>
    /// <returns>Студент успешно удалён</returns>
    /// <response code="404">Студент не найден</response>
    /// <responce code="204">Студент успешно удалён</responce>
    /// <response code="500">Внутренняя ошибка сервера</response>
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        var exist = await _query.GetAsync(id);

        if (exist == null)
        {
            return NotFound();
        }

        var command = new DeleteStudentCommand(id);

        try
        {
            var result = await _mediator.Send(command);

            return result ? NoContent() : StatusCode((int)HttpStatusCode.InternalServerError);
        }
        catch (Exception ex)
        {
            return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
        }
    }
}
