﻿using System;
using MediatR;

namespace SoftMediaLab.Commands;

public class UpdateStudentCommand : IRequest<bool>
{
    public UpdateStudentCommand(int id, string fullName, string performance, DateTime birthDate)
    {
        Id = id;
        FullName = fullName;
        BirthDate = birthDate;
        Performance = performance;
    }

    public int Id { get; private set; }
    public string FullName { get; private set; }
    public string Performance { get; private set; }
    public DateTime BirthDate { get; private set; }
}
