﻿using MediatR;

namespace SoftMediaLab.Commands;

public class DeleteStudentCommand : IRequest<bool>
{
    public DeleteStudentCommand(int id)
    {
        Id = id;
    }

    public int Id { get; private set; }
}
