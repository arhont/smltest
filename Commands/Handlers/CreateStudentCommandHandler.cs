﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SoftMediaLab.Infrastructure;
using SoftMediaLab.Models;

namespace SoftMediaLab.Commands.Handlers;

public class CreateStudentCommandHandler : IRequestHandler<CreateStudentCommand, bool>
{
    private readonly IStudentRepository _repo;

    public CreateStudentCommandHandler(IStudentRepository repo)
    {
        _repo = repo;
    }

    public async Task<bool> Handle(CreateStudentCommand request, CancellationToken token)
    {
        var student = new Student()
        {
            Performance = (Performance)Enum.Parse(typeof(Performance), request.Performance),
            FullName = request.FullName,
            BirthDate = request.BirthDate
        };

        _repo.Add(student);

        return await _repo.SaveChangesAsync();
    }
}
