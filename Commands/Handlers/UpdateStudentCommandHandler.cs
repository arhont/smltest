﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SoftMediaLab.Infrastructure;
using SoftMediaLab.Models;

namespace SoftMediaLab.Commands.Handlers;

public class UpdateStudentCommandHandler : IRequestHandler<UpdateStudentCommand, bool>
{
    private readonly IStudentRepository _repo;

    public UpdateStudentCommandHandler(IStudentRepository repo)
    {
        _repo = repo;
    }

    public async Task<bool> Handle(UpdateStudentCommand request, CancellationToken token)
    {
        var exist = await _repo.GetAsync(request.Id);

        if (exist == null)
        {
            throw new Exception("Not found");
        }

        exist.Performance = (Performance)Enum.Parse(typeof(Performance), request.Performance);
        exist.BirthDate = request.BirthDate;
        exist.FullName = request.FullName;

        _repo.Update(exist);

        return await _repo.SaveChangesAsync();
    }
}
