﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SoftMediaLab.Infrastructure;

namespace SoftMediaLab.Commands.Handlers;

public class DeleteStudentCommandHandler : IRequestHandler<DeleteStudentCommand, bool>
{
    private readonly IStudentRepository _repo;

    public DeleteStudentCommandHandler(IStudentRepository repo)
    {
        _repo = repo;
    }

    public async Task<bool> Handle(DeleteStudentCommand request, CancellationToken token)
    {
        var exist = await _repo.GetAsync(request.Id);

        _repo.Delete(exist);

        return await _repo.SaveChangesAsync();
    }
}
