﻿using System;
using MediatR;

namespace SoftMediaLab.Commands;

public class CreateStudentCommand : IRequest<bool>
{
    public CreateStudentCommand(string fullName, string performance, DateTime birthDate)
    {
        FullName = fullName;
        BirthDate = birthDate;
        Performance = performance;
    }

    public string FullName { get; private set; }
    public string Performance { get; private set; }
    public DateTime BirthDate { get; private set; }
}
