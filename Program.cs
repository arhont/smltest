using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SoftMediaLab.Extensions;
using SoftMediaLab.Infrastructure;

namespace SoftMediaLab;

public class Program
{
    public static void Main(string[] args)
    {
        var host = BuildWebHost(args);
        CreateDbIfNotExists(host);

        host.MigrateDbContext<SmlContext>((context, services) =>
        {
            var env = services.GetService<IWebHostEnvironment>();

            var logger = services.GetService<ILogger<SmlContextSeed>>();

            new SmlContextSeed().SeedAsync(context, logger)
                .Wait();
        });
        host.Run();
    }

    public static IWebHost BuildWebHost(string[] args)
    {
        return WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>()
            .UseContentRoot(Directory.GetCurrentDirectory())
            .ConfigureAppConfiguration((builderContext, config) =>
            {
                var configurationBuilder = new ConfigurationBuilder();
                configurationBuilder.AddEnvironmentVariables();
                config.AddConfiguration(configurationBuilder.Build());
            })
            .ConfigureLogging((hostingContext, builder) =>
            {
                builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                builder.AddConsole();
                builder.AddDebug();
            })
            .Build();
    }

    private static void CreateDbIfNotExists(IWebHost host)
    {
        using (var scope = host.Services.CreateScope())
        {
            var services = scope.ServiceProvider;

            try
            {
                var context = services.GetRequiredService<SmlContext>();
                context.Database.EnsureCreated();
            }
            catch (Exception ex)
            {
                var logger = services.GetRequiredService<ILogger<Program>>();
                logger.LogError(ex, "An error occurred creating the DB.");
            }
        }
    }
}
