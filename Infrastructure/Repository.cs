﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SoftMediaLab.Models;

namespace SoftMediaLab.Infrastructure;

public class Repository : IStudentRepository
{
    private readonly SmlContext _context;
    private readonly DbSet<Student> _dbSet;

    public Repository(SmlContext context)
    {
        _context = context;
        _dbSet = _context.Set<Student>();
    }

    public async Task<Student> GetAsync(int id)
    {
        return await _dbSet.FindAsync(id);
    }

    public async Task<List<Student>> GetAsync()
    {
        return await _dbSet.AsNoTracking().ToListAsync();
    }

    public async Task<List<Student>> GetAsync(Expression<Func<Student, bool>> predicate)
    {
        return await _dbSet.AsNoTracking().Where(predicate).ToListAsync();
    }

    public void Add(Student item)
    {
        _dbSet.Add(item);
    }

    public void Update(Student item)
    {
        _dbSet.Update(item);
    }

    public void Delete(Student item)
    {
        _dbSet.Remove(item);
    }

    public async Task<bool> SaveChangesAsync()
    {
        return await _context.SaveChangesAsync() == 1;
    }
}
