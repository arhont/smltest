﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SoftMediaLab.Models;

namespace SoftMediaLab.Infrastructure;

public class StudentQuery : IStudentQuery
{
    private readonly IStudentRepository _repo;

    public StudentQuery(IStudentRepository repo)
    {
        _repo = repo;
    }

    private static StudentViewModel EntityToViewModel(Student entity)
    {
        if (entity == null)
        {
            return null;
        }

        var vm = new StudentViewModel()
        {
            Id = entity.Id,
            FullName = entity.FullName,
            BirthDate = entity.BirthDate ?? DateTime.UtcNow,
            Performance = entity.Performance.ToString()
        };

        return vm;

    }

    public async Task<StudentViewModel> GetAsync(int id)
    {
        return EntityToViewModel(await _repo.GetAsync(id));
    }

    public async Task<List<StudentViewModel>> GetAsync()
    {
        return (await _repo.GetAsync()).Select(EntityToViewModel).ToList();
    }

    public async Task<List<StudentViewModel>> GetAsync(Expression<Func<Student, bool>> predicate)
    {
        return (await _repo.GetAsync(predicate)).Select(EntityToViewModel).ToList();
    }
}
