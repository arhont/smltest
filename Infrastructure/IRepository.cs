﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SoftMediaLab.Models;

namespace SoftMediaLab.Infrastructure;

public interface IRepository<TEntity> where TEntity : class
{
    Task<TEntity> GetAsync(int id);
    Task<List<TEntity>> GetAsync();
    Task<List<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate);
    void Add(TEntity item);
    void Update(TEntity item);
    void Delete(TEntity item);
    Task<bool> SaveChangesAsync();
}

public interface IStudentRepository : IRepository<Student>
{
}
