﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SoftMediaLab.Models;

namespace SoftMediaLab.Infrastructure;

public interface IQuery<TEntity, TViewModel> where TEntity : class
    where TViewModel : class
{
    Task<TViewModel> GetAsync(int id);
    Task<List<TViewModel>> GetAsync();
    Task<List<TViewModel>> GetAsync(Expression<Func<TEntity, bool>> predicate);
}

public interface IStudentQuery : IQuery<Student, StudentViewModel>
{
}
