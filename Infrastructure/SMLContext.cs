﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Polly;
using SoftMediaLab.Models;

namespace SoftMediaLab.Infrastructure;

public class SmlContext : DbContext
{
    public SmlContext(DbContextOptions<SmlContext> options) : base(options)
    {
    }

    public DbSet<Student> Students { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Student>()
            .ToTable("Students")
            .HasKey(r => r.Id);
        builder.Entity<Student>()
            .Property(r => r.Performance)
            .HasDefaultValue(Performance.Неуд);
    }
}

public class SmlContextSeed : BaseContextSeed<SmlContext>
{
    public override async Task SeedAsync(SmlContext context, ILogger<BaseContextSeed<SmlContext>> logger)
    {
        var policy = CreatePolicy(logger, nameof(SmlContext));

        await policy.ExecuteAsync(async () =>
        {
            if (!context.Students.Any())
            {
                var students = Enumerable.Range(0, 10).Select(r => new Student()
                {
                    Performance = Performance.Неуд,
                    FullName = $"Test - {r}",
                    BirthDate = DateTime.UtcNow
                });

                var student = new Student()
                {
                    Performance = Performance.Неуд,
                    FullName = "Test",
                    BirthDate = DateTime.UtcNow
                };

                context.Students.Add(student);
                context.Students.AddRange(students);

                await context.SaveChangesAsync();
            }
        });
    }
}

public abstract class BaseContextSeed<TContextType>
{
    protected static AsyncPolicy CreatePolicy(ILogger<BaseContextSeed<TContextType>> logger, string prefix, int retries = 3)
    {
        return Policy.Handle<SqlException>().WaitAndRetryAsync
        (
            retries,
            retry => TimeSpan.FromSeconds(5),
            (exception, timeSpan, retry, ctx) =>
            {
                logger.LogTrace(
                    $"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
            }
        );
    }

    public abstract Task SeedAsync(TContextType context, ILogger<BaseContextSeed<TContextType>> logger);
}
