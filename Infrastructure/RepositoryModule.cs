﻿using Autofac;

namespace SoftMediaLab.Infrastructure;

public class RepositoryModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterType<StudentQuery>()
            .As<IStudentQuery>().InstancePerLifetimeScope();

        builder.RegisterType<Repository>()
            .As<IStudentRepository>().InstancePerLifetimeScope();
    }
}
